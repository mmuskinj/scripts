INPUT_DIR=$1
OUTPUT_DIR=$2

mkdir -p $OUTPUT_DIR

echo "Extracting all .tgz files in ${INPUT_DIR} and placing them in ${OUTPUT_DIR}"

for file in $INPUT_DIR/*.tgz; do
  echo "Extracting $file"
  tar -zxf $file -C $OUTPUT_DIR;
done
