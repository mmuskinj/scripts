INPUT_DIR=$1
temp_file=$(mktemp)

for file in `find $INPUT_DIR/ | grep log.EVNTtoHITS`; do
  # echo $file
  cat $file | grep -i "inputEVNTFile = \[" | sed -r "s/.*inputEVNTFile = \[(.*)\]/\1/" >> $temp_file
done

cat $temp_file | sort | uniq -c
rm -R $temp_file


