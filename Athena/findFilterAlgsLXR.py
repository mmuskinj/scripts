import urllib
import re
import sys
import time

def parse(hits):
    if len(hits) == 0:
        return
        
    page2 = ""
    
    results = []
    
    for hit in hits:
        noMatch = True
        line = re.findall('#([0-9]+)', hit)[0]
        url = "https://acode-browser1.usatlas.bnl.gov%s" % hit[:-5]
        page = urllib.urlopen(url).readlines()
        page2 = page
        for l in page:
            out = re.findall('name="%s">%s</a>.*href="/lxr/ident\?_i(.*) \+=' % (line, line), l)
            if len(out):
                noMatch = False
                print "+++++ https://acode-browser1.usatlas.bnl.gov"+hit
                print out
                seqName = re.findall('(>[a-zA-Z0-9]*</a>)',out[0])[0]
                for l2 in page2:
                    if seqName in l2:
                        matches = re.findall("(%s).*>([a-zA-Z0-9]*)</a>(.*)" % seqName, l2)
                        print matches
                        results += [matches]
                        break
        if noMatch:
            print "https://acode-browser1.usatlas.bnl.gov"+hit
    
    return results

def main(argv):
    
    page = urllib.urlopen("https://acode-browser1.usatlas.bnl.gov/lxr/ident?_i=%s" % argv[0]).readlines()
    
    result = {}

    for l in page:
        if "a class='identline' href=" in l:
            hits = re.findall('href="(/lxr/source/athena/[a-zA-Z0-9/\._]*\.py#[0-9]*)"',l)
            print "--------------"
            res = parse(hits)
            if res:
                for elem in res:
                    key = elem[0]
                    if key in result.keys():
                        result[key] += 1
                    else:
                        result[key] = 1
        
    for k,v in result.iteritems():
        print k, " ", v
        
if __name__ == "__main__":
   main(sys.argv[1:])