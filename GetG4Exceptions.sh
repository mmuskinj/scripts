INPUT_DIR=$1
temp_file=$(mktemp)

for file in `find $INPUT_DIR/ | grep log.EVNTtoHITS`; do
  echo $file
  cat $file | grep -i "\* G4Exception :" >> $temp_file
done

cat $temp_file | sed 's/.*G4Exception : \(.*\)/\1/g' | sort | uniq -c
rm -R $temp_file


