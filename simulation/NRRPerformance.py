import ROOT
ROOT.gROOT.SetBatch(True)

import os

AtlasStylePath = os.environ['HOME'] + '/RootUtils/AtlasStyle'
AtlasStyle = os.path.exists(AtlasStylePath)

if AtlasStyle:
    print "setting ATLAS Style"
    ROOT.gROOT.LoadMacro(AtlasStylePath + "/AtlasStyle.C")
    ROOT.gROOT.LoadMacro(AtlasStylePath + "/AtlasLabels.C")
    ROOT.gROOT.LoadMacro(AtlasStylePath + "/AtlasUtils.C")
    ROOT.SetAtlasStyle()
    
    
graph1 = {
0   : [180118.020, 7458.596],
1   : [167099.646, 6695.689],
2.5 : [158283.343, 6490.342],
5   : [155334.374, 6172.183],
7.5 : [151898.495, 6032.739],
10  : [150764.737, 6004.604],
}

graph2 = {
0   : [231232.495, 9167.961],
1   : [206027.939, 8003.510],
2.5 : [213086.152, 8745.862],
5   : [193285.465, 8031.401],
7.5 : [194862.343, 8167.051],
10  : [200102.101, 7994.251],
}

graph3 = {
0   : [166004.566, 6739.854],
1   : [152464.293, 6072.020],
5   : [141156.465, 5755.344],
10  : [139645.687, 5922.180],
}

graph4 = {
0   : [212207.697, 8658.688],
1   : [202395.374, 8202.421],
10  : [175610.081, 7087.191],
}

gr1 = ROOT.TGraphErrors()
for k,v in sorted(graph1.iteritems()):
    if not v: continue
    gr1.SetPoint(gr1.GetN(), k-0.1, 100.*v[0]/graph1[0][0])
    gr1.SetPointError(gr1.GetN()-1, 0, 100.*v[1]/graph1[0][0])

gr2 = ROOT.TGraphErrors()
for k,v in sorted(graph2.iteritems()):
    if not v: continue
    gr2.SetPoint(gr2.GetN(), k+0.05, 100.*v[0]/graph2[0][0])
    gr2.SetPointError(gr2.GetN()-1, 0, 100.*v[1]/graph2[0][0])
    
gr3 = ROOT.TGraphErrors()
for k,v in sorted(graph3.iteritems()):
    if not v: continue
    gr3.SetPoint(gr3.GetN(), k-0.05, 100.*v[0]/graph1[0][0])
    gr3.SetPointError(gr3.GetN()-1, 0, 100.*v[1]/graph1[0][0])
    
gr4 = ROOT.TGraphErrors()
for k,v in sorted(graph4.iteritems()):
    if not v: continue
    gr4.SetPoint(gr4.GetN(), k+0.1, 100.*v[0]/graph2[0][0])
    gr4.SetPointError(gr4.GetN()-1, 0, 100.*v[1]/graph2[0][0])

MARKER_SIZE = 1.5
    
gr1.SetLineColor(ROOT.kBlue-7)
gr1.SetMarkerColor(ROOT.kBlue-7)
gr1.SetMarkerSize(MARKER_SIZE)
gr1.SetLineWidth(2)

gr2.SetLineColor(ROOT.kRed+2)
gr2.SetMarkerColor(ROOT.kRed+2)
gr2.SetMarkerSize(MARKER_SIZE)
gr2.SetLineWidth(2)

gr3.SetLineColor(ROOT.kBlue-7)
gr3.SetMarkerColor(ROOT.kBlue-7)
gr3.SetMarkerStyle(24)
gr3.SetMarkerSize(MARKER_SIZE)
gr3.SetLineWidth(2)

gr4.SetLineColor(ROOT.kRed+2)
gr4.SetMarkerColor(ROOT.kRed+2)
gr4.SetMarkerStyle(24)
gr4.SetMarkerSize(MARKER_SIZE)
gr4.SetLineWidth(2)

grTemp = ROOT.TGraphErrors()
grTemp.SetMarkerStyle(24)
grTemp.SetMarkerSize(MARKER_SIZE)
grTemp.SetLineWidth(2)
    
mg = ROOT.TMultiGraph()
mg.Add(gr1,"pe")
mg.Add(gr2,"pe")
mg.Add(gr3,"pe")
mg.Add(gr4,"pe")

leg = ROOT.TLegend(0.6, 0.74, 0.9, 0.92)
leg.SetBorderSize(0)
leg.SetFillColor(0)
leg.SetFillStyle(0)
leg.SetTextSize(0.045)
leg.AddEntry(gr1,"#font[42]{aiatlas023}")
leg.AddEntry(gr2,"#font[42]{local T3}")
leg.AddEntry(grTemp,"#font[42]{with EM range cuts}")

canv = ROOT.TCanvas("canv","canv",800,600)
mg.Draw("a")
mg.SetMaximum(117)
mg.SetMinimum(70)
mg.GetXaxis().SetLimits(-1,11)
mg.GetXaxis().SetTitle("NRR Energy threshold [MeV]")
mg.GetYaxis().SetTitle("CPU time per event [%]")
if AtlasStyle:
    ROOT.ATLASLabel(0.18, 0.88, "Simulation Internal", 1)
    ROOT.myText(0.18, 0.82, 1, "#sqrt{s}=13 TeV, 100 t#bar{t} events")
    ROOT.myText(0.18, 0.76, 1, "NRR weight = 10")
leg.Draw()

l1 = ROOT.TLine(-1, 90, 11, 90)
l1.SetLineStyle(2)
# l1.SetLineColor(ROOT.kGray-1)
l1.Draw()

l2 = ROOT.TLine(-1, 80, 11, 80)
l2.SetLineStyle(2)
# l2.SetLineColor(ROOT.kGray-1)
l2.Draw()

canv.Print("NRRPerformance.eps")
