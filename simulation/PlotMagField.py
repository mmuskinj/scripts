import os
import ROOT
import pickle

ROOT.gROOT.SetBatch(True)


AtlasStylePath = os.environ['HOME'] + '/RootUtils/AtlasStyle'
AtlasStyle = os.path.exists(AtlasStylePath)
if AtlasStyle:
    ROOT.gROOT.LoadMacro(AtlasStylePath + "/AtlasStyle.C")
    ROOT.gROOT.LoadMacro(AtlasStylePath + "/AtlasLabels.C")
    ROOT.gROOT.LoadMacro(AtlasStylePath + "/AtlasUtils.C")
    ROOT.SetAtlasStyle()


f = ROOT.TFile(
    "/afs/f9.ijs.si/home/miham/Simulation/MagField/run/NewMagField.root", "READ")

zaxis = [
-3600,
-2880,
-2160,
-1440,
-720,
 0,
 720,
 1440,
 2160,
 2880,
 3600,
]

for i in range(11):

    z = zaxis[i]

    # hxy = ROOT.TGraph2D()
    hxy = ROOT.TH2F("proxy%s" % z,"proxy%s" % z,1000,-5000,5000,1000,-5000,5000)

    # pickle_in = open("graph.pickle","rb")
    # hxy = pickle.load(pickle_in)

    # hxy.SetNpy(500)
    # hxy.SetNpx(500)

    t = f.Get("field")

    n = 0
    for e in t:
        # if n > 100000:
        #     break
        if e.z != z:
            continue
        n += 1
        if n%100000 == 0:
            print z," ",n," ",e.z
        hxy.SetBinContent(hxy.FindBin(e.x,e.y),((e.fx)**2+(e.fy)**2+(e.fz)**2)**(0.5))


    pickle_out = open("graph%s.pickle" % z,"wb")
    pickle.dump(hxy, pickle_out)
    pickle_out.close()

    cxy = ROOT.TCanvas("cxy%s" % z, "cxy%s" % z, 800, 600)
    cxy.cd()
    cxy.SetRightMargin(0.24)
    # cxy.SetLeftMargin(0.25) 
    # cxy.SetBottomMargin(0.25)
    cxy.SetTopMargin(0.07)
    # proxy_histo.Draw()
    hxy.GetXaxis().SetTitle("x [mm]")
    hxy.GetXaxis().SetTitleOffset(0)
    hxy.GetXaxis().SetNdivisions(505)
    hxy.GetYaxis().SetNdivisions(505)
    hxy.GetYaxis().SetTitle("y [mm]")
    hxy.GetZaxis().SetTitle("|B|")
    hxy.GetZaxis().SetTitleOffset(1.5)
    hxy.Draw("COLZ")
    if AtlasStyle:
        ROOT.ATLASLabel(0.18, 0.87, "Internal", 1)
        ROOT.myText(0.18,0.81,1,"z = %s mm" % z)
    ROOT.gPad.RedrawAxis()
    cxy.Print("xy%s.png" % z)
    if i == 0:
        cxy.Print("xy.pdf(")
    elif i == 8:
        cxy.Print("xy.pdf)")
    else:
        cxy.Print("xy.pdf")
    
