import os
if not os.path.exists('workers'):
    os.makedirs('workers')
import re
import math
import pickle

from collections import defaultdict

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gErrorIgnoreLevel = ROOT.kWarning

AtlasStylePath = os.environ['HOME'] + '/RootUtils/AtlasStyle'
AtlasStyle = os.path.exists(AtlasStylePath)
if AtlasStyle:
    ROOT.gROOT.LoadMacro(AtlasStylePath + "/AtlasStyle.C")
    ROOT.gROOT.LoadMacro(AtlasStylePath + "/AtlasLabels.C")
    ROOT.gROOT.LoadMacro(AtlasStylePath + "/AtlasUtils.C")
    ROOT.SetAtlasStyle()

# Example G4Exception
#--------------------
"""
-------- WWWW ------- G4Exception-START -------- WWWW -------
*** G4Exception : GeomNav1002
      issued by : G4PropagatorInField::ComputeStep
 Unfinished integration of track (likely looping particle)   of momentum (203.47,-291.404,-361.612) ( magnitude = 507.03 )
 after 1000 field substeps  totaling 27.9393978834 mm  out of requested step 28.0458270736 mm  a fraction of 99.62052 %
 in volume LAr::Barrel::Cryostat::Cylinder::#12Phys with material Aluminium ( density = 2.7 g / cm^3 )
*** This is just a warning message. ***
-------- WWWW -------- G4Exception-END --------- WWWW -------
"""


def merge_histos(*dicts):
    dict0 = {}
    for d in dicts:
        for k, v in d.items():
            if not k in dict0.keys():
                dict0[k] = v
            else:
                dict0[k].Add(v)
    return dict0

def dsum(*dicts):
    ret = defaultdict(int)
    for d in dicts:
        for k, v in d.items():
            ret[k] += v
    return dict(ret)

class G4ExceptionParser:

    def __add__(self, other):
        assert self.directory == other.directory
        out = G4ExceptionParser(self.directory, "merged")

        out.events = self.events + other.events

        out.mag = self.mag
        out.pt = self.pt
        out.total = self.total
        out.requested = self.requested
        out.mag.Add(other.mag)
        out.pt.Add(other.pt)
        out.total.Add(other.total)
        out.requested.Add(other.requested)

        out.issuers = dsum(self.issuers, other.issuers)
        out.volumes = dsum(self.volumes, other.volumes)
        out.materials = dsum(self.materials, other.materials)

        out.pt_vol = merge_histos(self.pt_vol, other.pt_vol)
        out.mag_vol = merge_histos(self.mag_vol, other.mag_vol)
        out.tot_vol = merge_histos(self.tot_vol, other.tot_vol)
        out.req_vol = merge_histos(self.req_vol, other.req_vol)
        out.pt_mat = merge_histos(self.pt_mat, other.pt_mat)
        out.mag_mat = merge_histos(self.mag_mat, other.mag_mat)
        out.tot_mat = merge_histos(self.tot_mat, other.tot_mat)
        out.req_mat = merge_histos(self.req_mat, other.req_mat)

        return out

    def __init__(self, directory, name, skip=0, maxfiles=-1):
        self.directory = directory
        self.skip = skip
        self.maxfiles = maxfiles
        self.name = name
        self.issuers = defaultdict(int)
        self.volumes = defaultdict(int)
        self.materials = defaultdict(int)
        self.mag = ROOT.TH1F("mag"+name, "mag"+name, 1000, -3, 9)
        self.pt = ROOT.TH1F("pt"+name, "pt"+name, 1000, -3, 9)
        self.total = ROOT.TH1F("total"+name, "total"+name, 1000, -3, 6)
        self.requested = ROOT.TH1F("requested"+name, "requested"+name, 1000, -3, 6)
        self.pt_vol = {}
        self.mag_vol = {}
        self.tot_vol = {}
        self.req_vol = {}
        self.pt_mat = {}
        self.mag_mat = {}
        self.tot_mat = {}
        self.req_mat = {}
        self.events = 0

    def loop(self):
        count = 0
        skipped = 0
        for subdir, dirs, files in os.walk(self.directory):
            for file in files:
                if file == "log.EVNTtoHITS":
                    if self.skip > 0 and skipped < self.skip:
                        skipped += 1
                        continue
                    count += 1
                    if self.maxfiles > 0 and count > self.maxfiles:
                        return
                    print self.name,"\t:\t",os.path.join(subdir, file)
                    with open(os.path.join(subdir, file)) as f:
                        self.parse(f)

    def parse(self, f):
        for l in f:
            if "GeomNav1002" in l:
                self.parseGeomNav(f, l)
            if "INFO Events processed:" in l:
                events = re.findall("INFO Events processed: ([0-9]*)",l)
                assert len(events)==1, [l, events]
                self.events += float(events[0])

    def parseGeomNav(self, f, l0):
        l1 = f.next()
        if "issued by :" in l1:
            self.parseL1(l1)
        else:
            print l1
            return

        l2 = f.next()
        if "Unfinished integration of track (likely looping particle)   of momentum" in l2:
            mag, pt = self.parseL2(l2)
        else:
            print l2
            return

        l3 = f.next()
        if "after 1000 field substeps  totaling " in l3:
            tot, req = self.parseL3(l3)
        else:
            print l3
            return

        l4 = f.next()
        if "in volume" in l4 and "with material" in l4:
            vol, mat = self.parseL4(l4)
        else:
            print l4
            return

        l5 = f.next()
        if not " *** This" in l5:
            print "WARNING suspicious last line..."
            print "".join([l0, l1, l2, l3, l4, l5])

        self.Fill(vol, mat, mag, pt, tot, req)

    def parseL1(self, l1):
        issuer = re.findall("issued by : (.*)", l1)
        assert len(issuer) == 1, l1
        self.issuers[issuer[0]] += 1

    def parseL2(self, l2):
        magnitude = re.findall("magnitude = ([0-9\.e\-\+]+)", l2)
        if not len(magnitude) == 1:
            print "warning: ",l2
            return -100, -100
        magnitude = float(magnitude[0])
        self.mag.Fill(math.log10(magnitude))

        momentum = re.findall(
            "momentum \(([0-9\.\-e\+]+),([0-9\.\-e\+]+),([0-9\.\-e\+]+)\)", l2)
        if not len(momentum) == 1:
            print "warning: ",l2
            return math.log10(magnitude), -100
        if not len(momentum[0]) == 3:
            print "warning: ",l2
            return math.log10(magnitude), -100
        pt = (float(momentum[0][0])**2 + float(momentum[0][1])**2)**(0.5)
        self.pt.Fill(math.log10(pt))

        return math.log10(magnitude), math.log10(pt)

    def parseL3(self, l3):
        distance = re.findall(
            "totaling ([0-9\.]*) mm  out of requested step ([0-9\.]*)", l3)
        assert len(distance) == 1, [l3, distance]
        assert len(distance[0]) == 2, [l3, distance]

        self.total.Fill(math.log10(float(distance[0][0])))
        self.requested.Fill(math.log10(float(distance[0][1])))

        return float(distance[0][0]), float(distance[0][1])

    def parseL4(self, l4):
        volume = re.findall("in volume (.*) with material", l4)
        assert len(volume) == 1, l4
        self.volumes[volume[0]] += 1

        material = re.findall("with material ([a-zA-Z0-9\.\:\_]*) ", l4)
        assert len(material) == 1, l4
        self.materials[material[0]] += 1

        return volume[0], material[0]

    def Fill(self, vol, mat, mag, pt, tot, req):
        if not vol in self.pt_vol:
            self.pt_vol[vol] = ROOT.TH1F(
                self.name+"pt_%s" % vol, self.name+"pt_%s" % vol, 1000, -3, 9)
        self.pt_vol[vol].Fill(pt)

        if not vol in self.mag_vol:
            self.mag_vol[vol] = ROOT.TH1F(
                self.name+"mag_%s" % vol, self.name+"mag_%s" % vol, 1000, -3, 9)
        self.mag_vol[vol].Fill(mag)

        if not vol in self.tot_vol:
            self.tot_vol[vol] = ROOT.TH1F(
                self.name+"tot_%s" % vol, self.name+"tot_%s" % vol, 1000, -3, 6)
        self.tot_vol[vol].Fill(math.log10(tot))

        if not vol in self.req_vol:
            self.req_vol[vol] = ROOT.TH1F(
                self.name+"req_%s" % vol, self.name+"req_%s" % vol, 1000, -3, 6)
        self.req_vol[vol].Fill(math.log10(req))

        if not mat in self.pt_mat:
            self.pt_mat[mat] = ROOT.TH1F(
                self.name+"pt_%s" % mat, self.name+"pt_%s" % mat, 1000, -3, 9)
        self.pt_mat[mat].Fill(pt)

        if not mat in self.mag_mat:
            self.mag_mat[mat] = ROOT.TH1F(
                self.name+"mag_%s" % mat, self.name+"mag_%s" % mat, 1000, -3, 9)
        self.mag_mat[mat].Fill(mag)

        if not mat in self.tot_mat:
            self.tot_mat[mat] = ROOT.TH1F(
                self.name+"tot_%s" % mat, self.name+"tot_%s" % mat, 1000, -3, 6)
        self.tot_mat[mat].Fill(math.log10(tot))

        if not mat in self.req_mat:
            self.req_mat[mat] = ROOT.TH1F(
                self.name+"req_%s" % mat, self.name+"req_%s" % mat, 1000, -3, 6)
        self.req_mat[mat].Fill(math.log10(req))

    def Write(self, f):
        f.cd()
        self.mag.Write()
        self.pt.Write()
        self.total.Write()
        self.requested.Write()

        d = f.mkdir("volumes")
        d.cd()
        for k in self.pt_vol:
            self.pt_vol[k].Write()
            self.mag_vol[k].Write()
            self.tot_vol[k].Write()
            self.req_vol[k].Write()

        d = f.mkdir("materials")
        d.cd()
        for k in self.pt_mat:
            self.pt_mat[k].Write()
            self.mag_mat[k].Write()
            self.tot_mat[k].Write()
            self.req_mat[k].Write()

    def PlotFromDict(self, key, dict2, xtitle, file):
        h = dict2[key].Clone()
        canv = ROOT.TCanvas(h.GetName(), h.GetName(), 800, 600)
        canv.SetLogy()
        h.Draw()
        h.GetXaxis().SetTitle(xtitle)
        h.GetYaxis().SetTitle("Entries")
        if AtlasStyle:
            ROOT.ATLASLabel(0.18, 0.87, "Simulation Internal", 1)
            ROOT.myText(0.18, 0.81, 1, "#sqrt{s}=13 TeV, %s events" % self.events)
            ROOT.myText(0.18, 0.75, 1, key)
        canv.Print(file)

    def Plot(self, h, xtitle, file):
        canv = ROOT.TCanvas(h.GetName(), h.GetName(), 800, 600)
        canv.SetLogy()
        h.Draw()
        h.GetXaxis().SetTitle(xtitle)
        h.GetYaxis().SetTitle("Entries")
        if AtlasStyle:
            ROOT.ATLASLabel(0.18, 0.87, "Simulation Internal", 1)
            ROOT.myText(0.18, 0.81, 1, "#sqrt{s}=13 TeV, %s events" % self.events)
        canv.Print(file)

    def Print(self):
        count = 1
        for key, value in sorted(self.volumes.iteritems(), key=lambda (k, v): (v, k), reverse=True):
            fname = ".pdf"
            if count == 1:
                fname += "("
            elif count == len(self.volumes):
                fname += ")"
            self.PlotFromDict(key, self.pt_vol,
                      "log10( p_{T} [MeV] )", "output/pt_vol" + fname)
            self.PlotFromDict(key, self.mag_vol,
                      "log10( |P| [MeV] )", "output/mag_vol" + fname)
            self.PlotFromDict(key, self.tot_vol,
                      "log10( total length [mm] )", "output/tot_vol" + fname)
            self.PlotFromDict(key, self.req_vol,
                      "log10( requested length [mm] )", "output/req_vol" + fname)
            count += 1
        count = 1
        for key, value in sorted(self.materials.iteritems(), key=lambda (k, v): (v, k), reverse=True):
            fname = ".pdf"
            if count == 1:
                fname += "("
            elif count == len(self.materials):
                fname += ")"
            self.PlotFromDict(key, self.pt_mat,
                      "log10( p_{T} [MeV] )", "output/pt_mat" + fname)
            self.PlotFromDict(key, self.mag_mat,
                      "log10( |P| [MeV] )", "output/mag_mat" + fname)
            self.PlotFromDict(key, self.tot_mat,
                      "log10( total length [mm] )", "output/tot_mat" + fname)
            self.PlotFromDict(key, self.req_mat,
                      "log10( requested length [mm] )", "output/req_mat" + fname)
            count += 1

        self.Plot(self.pt, "log10( p_{T} [MeV] )", "output/pt.pdf")
        self.Plot(self.mag, "log10( |P| [MeV] )", "output/mag.pdf")
        self.Plot(self.total, "log10( total length [mm] )", "output/tot.pdf")
        self.Plot(self.requested, "log10( requested length [mm] )", "output/req.pdf")

        print "------------"
        print "name: ",self.name
        print "processed events: ",self.events
        print "------------"

        print "------------"
        print "issuers:"
        print "------------"
        print self.issuers

        print "------------"
        print "volumes:"
        print "------------"
        for key, value in sorted(self.volumes.iteritems(), key=lambda (k, v): (v, k), reverse=True):
            print "%60s: %s" % (key, value)

        print "------------"
        print "materials:"
        print "------------"
        for key, value in sorted(self.materials.iteritems(), key=lambda (k, v): (v, k), reverse=True):
            print "%60s: %s" % (key, value)


def worker(args):
    directory, skip, maxfiles, name = args
    obj = G4ExceptionParser(directory, skip = skip, maxfiles = maxfiles, name = name)
    obj.loop()
    return obj

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Parse G4Exceptions in EVNTtoHITS log files")
    parser.add_argument("-d", metavar='d', type=str,
                        help="directory containing log files")
    parser.add_argument("-n", metavar='n', type=int, default=20,
                        help="number of files per parser")
    parser.add_argument("-p", metavar='p', type=int, default=4,
                        help="number of processes")
    parser.add_argument("-m", metavar='m', type=int, default=-1,
                        help="max log files")
    args = parser.parse_args()

    nfiles = 0
    for subdir, dirs, files in os.walk(args.d):
        for file in files:
            if file == "log.EVNTtoHITS":
                nfiles += 1

    if (args.m > 0):
        nfiles = args.m

    print "number of log files to process: ", nfiles

    parsers = [(args.d, args.n*x, args.n, "worker%s"%x) for x in range(nfiles/args.n)]
    print parsers

    from multiprocessing import Pool
    pool = Pool(args.p)
    results = pool.map(worker, (parsers))
    pool.close()
    pool.join()

    result = None
    for x in results:
        if result == None:
            result = x
        else:
            result = result + x

    result.Print()
    outfile = ROOT.TFile("output/out.root", "RECREATE")
    result.Write(outfile)
    outfile.Close()
